LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity grayscale_custom_instruction is
  port(

    RGB_i  : in std_logic_vector(31 downto 0);

    GRAY_o : out std_logic_vector(31 downto 0));

end grayscale_custom_instruction;           


architecture behav of grayscale_custom_instruction is

  signal s_red_rs      : unsigned(31 downto 0);
  signal s_green_rs    : unsigned(31 downto 0);
  signal s_blue_rs     : unsigned(31 downto 0);

  
  signal s_red         : std_logic_vector(31 downto 0);
  signal s_green       : std_logic_vector(31 downto 0);
  signal s_blue        : std_logic_vector(31 downto 0);


  signal s_red_ls      : unsigned(31 downto 0);
  signal s_green_ls    : unsigned(31 downto 0);
  signal s_blue_ls     : unsigned(31 downto 0);    

  
  signal s_red_scale   : unsigned(31 downto 0);
  signal s_green_scale : unsigned(31 downto 0);
  signal s_blue_scale  : unsigned(31 downto 0);

  signal s_gray_sum    : unsigned(31 downto 0);
  
begin

  -- gray = (((rgb>>11)&0x1F)<<3)*77; 
  -- gray += (((rgb>>5)&0x3F)<<2)*151;
  -- gray += (((rgb>>0)&0x1F)<<3)*28; 
  -- gray = gray >> 8;

  
  --- TODO
  --- use the downto to make the fist shift,
  --- the mask and the second shift in
  --- a more clever way!
  --- This may lead to a speed improvement!!
  --- TODO
  

  -- first shift
  s_red_rs   <= shift_right(unsigned(RGB_i), 11);
  s_green_rs <= shift_right(unsigned(RGB_i), 5);
  s_blue_rs  <= shift_right(unsigned(RGB_i), 0);

  -- the and step
  s_red   <= std_logic_vector(s_red_rs)   and X"1F";
  s_green <= std_logic_vector(s_green_rs) and X"3F";
  s_blue  <= std_logic_vector(s_blue_rs)  and X"1F";

  -- second shift
  s_red_ls   <= shift_left(unsigned(s_red)  , 3);
  s_green_ls <= shift_left(unsigned(s_green), 2);
  s_blue_ls  <= shift_left(unsigned(s_blue) , 3);

  -- final multiplication
  s_red_scale   <= to_unsigned((to_integer(s_red_ls  ) * 77) , 32);
  s_green_scale <= to_unsigned((to_integer(s_green_ls) * 151), 32);
  s_blue_scale  <= to_unsigned((to_integer(s_blue_ls ) * 28) , 32);

  -- final sum of all colors
  s_gray_sum    <= to_unsigned((to_integer(s_red_scale) + to_integer(s_green_scale) + to_integer(s_blue_scale)), 32);

  -- last shift and output
  GRAY_o <= std_logic_vector(shift_right(s_gray_sum, 8));
  
end behav;
