LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity threshold_instruction is
  port(

    sobelx_i  : in std_logic_vector(31 downto 0);
    sobely_i  : in std_logic_vector(31 downto 0);

    result_o : out std_logic_vector(31 downto 0));

end threshold_instruction;           


architecture behav of threshold_instruction is

  signal s_sobel_x : signed(31 downto 0);
  signal s_sobel_y : signed(31 downto 0);
  
  signal s_sobel_x_res : signed(31 downto 0);
  signal s_sobel_y_res : signed(31 downto 0);

  signal sum_s : signed(31 downto 0);
  
begin

  -- value = sobel_x_result[arrayindex];
  -- sum = (value < 0) ? -value : value;
  -- value = sobel_y_result[arrayindex];
  -- sum += (value < 0) ? -value : value;
  -- sobel_result[arrayindex] = (sum > threshold) ? 0xFF : 0;
  s_sobel_x <= signed(sobelx_i(31 downto 0));  
  s_sobel_y <= signed(sobely_i(31 downto 0));  
  

  -- result x
  s_sobel_x_res <=
        s_sobel_x when s_sobel_x > 0 else
	-s_sobel_x;

  -- result y
  s_sobel_y_res <=
         s_sobel_y when s_sobel_y > 0 else
	-s_sobel_y;

  -- sum
  sum_s <= s_sobel_x_res + s_sobel_y_res;

  -- output
  result_o <= X"000000FF" when sum_s > 128 else X"00000000";

  
end behav;
