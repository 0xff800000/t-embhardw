=============== Structure ===============

- Projet eclipse dans /Software
- Projet quartus dans /Hardware/quartus_project
- Sources VHDL dans /Hardware/vhdl_modules

=============== Optimisations ===============

- Compilation GCC -O3
- Unroll for loops : sobel functions
- Activation de la cache : instruction & data cache 16kB 
- Branch prediction : dynamic 256 entries
- Custom instructions : grayscale, sobel threshold

=============== Mesures 7.01.19 ===============

Chaque fonctions ont été mesurées séparément

CPU @ 50MHz
Sobel : 147-149 cylces/pixel; ~1.695 FPS
Sobel RGB : 81-82 cylces/pixel; ~3.3 FPS
Grayscale : 13-14 cylces/pixel; ~19 FPS

=============== RAW output ===============
Sobel : 148
Sobel : 149
Sobel : 149
Sobel : 148
Sobel : 148
Sobel : 149
Sobel : 147
Sobel : 148
Sobel : 147
Sobel : 147
Sobel : 148
Sobel : 147
Sobel : 147
Sobel RGB : 81
Sobel RGB : 82
Sobel RGB : 81
Sobel RGB : 82
Sobel RGB : 82
Sobel RGB : 82
Sobel RGB : 82
Sobel RGB : 82
Sobel RGB : 82
Sobel RGB : 81
Sobel RGB : 82
Sobel RGB : 82
Gray : 14
Gray : 14
Gray : 14
Gray : 14
Gray : 13
Gray : 13
Gray : 13
Gray : 13
