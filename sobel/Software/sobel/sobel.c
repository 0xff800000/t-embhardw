/*
 * sobel.c
 *
 *  Created on: Sep 12, 2015
 *      Author: theo
 */

#include <stdlib.h>
#include <stdio.h>
#include <system.h>
#include <stdint.h>
#include <io.h>

const char gx_array[3][3] = {{-1,0,1},
                             {-2,0,2},
                             {-1,0,1}};
const char gy_array[3][3] = { {1, 2, 1},
                              {0, 0, 0},
                             {-1,-2,-1}};

short *sobel_x_result;
short *sobel_y_result;
unsigned short *sobel_rgb565;
unsigned char *sobel_result;
int sobel_width;
int sobel_height;

void init_sobel_arrays(int width , int height) {
	int loop;
	sobel_width = width;
	sobel_height = height;
	if (sobel_x_result != NULL)
		free(sobel_x_result);
	sobel_x_result = (short *)malloc(width*height*sizeof(short));
	if (sobel_y_result != NULL)
		free(sobel_y_result);
	sobel_y_result = (short *)malloc(width*height*sizeof(short));
	if (sobel_result != NULL)
		free(sobel_result);
	sobel_result = (unsigned char *)malloc(width*height*sizeof(unsigned char));
	if (sobel_rgb565 != NULL)
		free(sobel_rgb565);
	sobel_rgb565 = (unsigned short *)malloc(width*height*sizeof(unsigned short));
	for (loop = 0 ; loop < width*height ; loop++) {
		sobel_x_result[loop] = 0;
		sobel_y_result[loop] = 0;
		sobel_result[loop] = 0;
		sobel_rgb565[loop] = 0;
	}
}

//__attribute__((optimize("unroll-loops")))
//short sobel_mac( unsigned char *pixels,
//                 int x,
//                 int y,
//                 const char *filter,
//                 unsigned int width ) {
//   short dy,dx;
//   short result = 0;
//   for (dy = -1 ; dy < 2 ; dy++) {
//      for (dx = -1 ; dx < 2 ; dx++) {
//         result += filter[(dy+1)*3+(dx+1)]*
//                   pixels[(y+dy)*width+(x+dx)];
//      }
//   }
//   return result;
//}

short sobel_mac( unsigned char *pixels,
                 int x,
                 int y,
                 const char *filter,
                 unsigned int width ) {
   short dy,dx;
   short result = 0;
   int val1 = (y-1)*width;
   int val2 = (y)*width;
   int val3 = (y+1)*width;
   if(filter == (const char *)gx_array){
		// dy = -1
		result -= pixels[val1+(x-1)]; // dx = -1 filter[0]=-1
//		result += filter[1]*pixels[val1+(x)]; // dx = 0 filter[1]=0
		result += pixels[val1+(x+1)]; // dx = 1 filter[2] = 1
		// dy = 0
		result -= pixels[val2+(x-1)]<<1; // dx = -1 filter[3]=-2
		//result += filter[4]*pixels[val2+(x)]; // dx = 0 filter[4]=0
		result += pixels[val2+(x+1)]<<1; // dx = 1 filter[5]=2
		// dy = 1
		result -= pixels[val3+(x-1)]; // dx = -1 filter[6]=-1
//		result += filter[7]*pixels[val3+(x)]; // dx = 0 filter[7]=0
		result += pixels[val3+(x+1)]; // dx = 1 filter[8]=1
   }
   else if(filter == (const char *)gy_array){
		// dy = -1
		result += pixels[val1+(x-1)]; // dx = -1 filter[0]=1
		result += pixels[val1+(x)]<<1; // dx = 0 filter[1]=2
		result += pixels[val1+(x+1)]; // dx = 1 filter[2] = 1
		// dy = 0
//		result -= pixels[val2+(x-1)]<<1; // dx = -1 filter[3]=0
		//result += filter[4]*pixels[val2+(x)]; // dx = 0 filter[4]=0
//		result += pixels[val2+(x+1)]<<2; // dx = 1 filter[5]=0
		// dy = 1
		result -= pixels[val3+(x-1)]; // dx = -1 filter[6]=-1
		result -= pixels[val3+(x)]<<1; // dx = 0 filter[7]=-2
		result -= pixels[val3+(x+1)]; // dx = 1 filter[8]=-1
   }
   else{
//	   printf("DEFAULT\n");
		// dy = -1
		result += filter[0]*pixels[val1+(x-1)]; // dx = -1
		result += filter[1]*pixels[val1+(x)]; // dx = 0
		result +=           pixels[val1+(x+1)]; // dx = 1 filter[2] = 1
		// dy = 0
		result += filter[3]*pixels[val2+(x-1)]; // dx = -1

		//result += filter[4]*pixels[val2+(x)]; // dx = 0; filter[4] = 0

		result += filter[5]*pixels[val2+(x+1)]; // dx = 1
		// dy = 1
		result -=           pixels[val3+(x-1)]; // dx = -1 filter[6] = -1
		result += filter[7]*pixels[val3+(x)]; // dx = 0
		result += filter[8]*pixels[val3+(x+1)]; // dx = 1
   }

//   for (dy = -1 ; dy < 2 ; dy++) {
//      for (dx = -1 ; dx < 2 ; dx++) {
//         result += filter[(dy+1)*3+(dx+1)]*
//                   pixels[(y+dy)*width+(x+dx)];
//      }
//   }
   return result;
}

__attribute__((optimize("unroll-loops")))
void sobel_x( unsigned char *source ) {
   int x,y;

   for (y = 1 ; y < (sobel_height-1) ; y++) {
      for (x = 1 ; x < (sobel_width-1) ; x++) {
         sobel_x_result[y*sobel_width+x] = sobel_mac(source,x,y,gx_array,sobel_width);
      }
   }
}

__attribute__((optimize("unroll-loops")))
void sobel_x_with_rgb( unsigned char *source ) {
   int x,y;
   short result;

   for (y = 1 ; y < (sobel_height-1) ; y++) {
      for (x = 1 ; x < (sobel_width-1) ; x++) {
    	  result = sobel_mac(source,x,y,gx_array,sobel_width);
          sobel_x_result[y*sobel_width+x] = result;
          if (result < 0) {
        	  sobel_rgb565[y*sobel_width+x] = ((-result)>>2)<<5;
          } else {
        	  sobel_rgb565[y*sobel_width+x] = ((result>>3)&0x1F)<<11;
          }
      }
   }
}

__attribute__((optimize("unroll-loops")))
void sobel_y( unsigned char *source ) {
   int x,y;

   for (y = 1 ; y < (sobel_height-1) ; y++) {
      for (x = 1 ; x < (sobel_width-1) ; x++) {
         sobel_y_result[y*sobel_width+x] = sobel_mac(source,x,y,gy_array,sobel_width);
      }
   }
}

__attribute__((optimize("unroll-loops")))
void sobel_y_with_rgb( unsigned char *source ) {
   int x,y;
   short result;

   for (y = 1 ; y < (sobel_height-1) ; y++) {
      for (x = 1 ; x < (sobel_width-1) ; x++) {
    	  result = sobel_mac(source,x,y,gy_array,sobel_width);
         sobel_y_result[y*sobel_width+x] = result;
         if (result < 0) {
       	  sobel_rgb565[y*sobel_width+x] = ((-result)>>2)<<5;
         } else {
       	  sobel_rgb565[y*sobel_width+x] = ((result>>3)&0x1F)<<11;
         }
      }
   }
}

__attribute__((optimize("unroll-loops")))
void sobel_threshold(const short threshold) {
	int x,y,arrayindex;
	short sum,value;
	for (y = 1 ; y < (sobel_height-1) ; y++) {
		for (x = 1 ; x < (sobel_width-1) ; x++) {
			arrayindex = (y*sobel_width)+x;
//			value = sobel_x_result[arrayindex];
//			sum = (value < 0) ? -value : value;
//			value = sobel_y_result[arrayindex];
//			sum += (value < 0) ? -value : value;
//			sobel_result[arrayindex] = (sum > threshold) ? 0xFF : 0;
			sobel_result[arrayindex] = ALT_CI_THRESHOLD_0((int8_t)sobel_x_result[arrayindex],(int8_t)sobel_y_result[arrayindex]);
		}
	}
}

unsigned short *GetSobel_rgb() {
	return sobel_rgb565;
}

unsigned char *GetSobelResult() {
	return sobel_result;
}
