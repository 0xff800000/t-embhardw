#include <stdio.h>
#include <system.h>
#include <stdlib.h>
#include <io.h>
#include "lcd_simple.h"
#include "grayscale.h"
#include "i2c.h"
#include "camera.h"
#include "vga.h"
#include "dipswitch.h"
#include "sobel.h"
#include "sys/alt_timestamp.h"
#include "alt_types.h"

int main()
{
  void *buffer1,*buffer2,*buffer3,*buffer4;
  unsigned short *image;
  unsigned char *grayscale;
  unsigned char current_mode;
  unsigned char mode;
  init_LCD();
  init_camera();
  vga_set_swap(VGA_QuarterScreen|VGA_Grayscale);
  printf("Hello from Nios II!\n");
  cam_get_profiling();
  buffer1 = (void *) malloc(cam_get_xsize()*cam_get_ysize());
  buffer2 = (void *) malloc(cam_get_xsize()*cam_get_ysize());
  buffer3 = (void *) malloc(cam_get_xsize()*cam_get_ysize());
  buffer4 = (void *) malloc(cam_get_xsize()*cam_get_ysize());
  cam_set_image_pointer(0,buffer1);
  cam_set_image_pointer(1,buffer2);
  cam_set_image_pointer(2,buffer3);
  cam_set_image_pointer(3,buffer4);
  enable_continues_mode();
  int CAM_SIZE_X = cam_get_xsize()>>1;
  int CAM_SIZE_Y = cam_get_ysize();
  init_sobel_arrays(CAM_SIZE_X,CAM_SIZE_Y);
  do {
	  if (new_image_available() != 0) {
		  if (current_image_valid()!=0) {
			  current_mode = DIPSW_get_value();
			  mode = current_mode&(DIPSW_SW1_MASK|DIPSW_SW3_MASK|DIPSW_SW2_MASK);
			  image = (unsigned short*)current_image_pointer();
		      switch (mode) {
		      case 0 : transfer_LCD_with_dma(&image[16520],
		                	CAM_SIZE_X,
		                	CAM_SIZE_Y,0);
		      	  	   if ((current_mode&DIPSW_SW8_MASK)!=0) {
		      	  		  vga_set_swap(VGA_QuarterScreen);
		      	  		  vga_set_pointer(image);
		      	  	   }
		      	  	   break;
		      case 1 :
		               {
						   alt_timestamp_start();
						   alt_u32 start_sobel_x = alt_timestamp();
						   conv_grayscale((void *)image,
						   		    		                  CAM_SIZE_X,
						   		    		                  CAM_SIZE_Y);
						   grayscale = get_grayscale_picture();


						   transfer_LCD_with_dma(&grayscale[16520],
											CAM_SIZE_X,
											CAM_SIZE_Y,1);
						   if ((current_mode&DIPSW_SW8_MASK)!=0) {
							  vga_set_swap(VGA_QuarterScreen|VGA_Grayscale);
							  vga_set_pointer(grayscale);
						   }

						   alt_u32 end_sobel_x = alt_timestamp();
						   printf("Gray : %d\n", (int)(end_sobel_x - start_sobel_x)/(CAM_SIZE_X*CAM_SIZE_Y));
		               }
		      	  	   break;
		      case 2 :
		               {
		            	   alt_timestamp_start();
						   alt_u32 start_sobel_x = alt_timestamp();

		            	   conv_grayscale((void *)image,
										  CAM_SIZE_X,
										  CAM_SIZE_Y);
						   grayscale = get_grayscale_picture();
						   sobel_x_with_rgb(grayscale);

						   image = GetSobel_rgb();
						   transfer_LCD_with_dma(&image[16520],
											CAM_SIZE_X,
											CAM_SIZE_Y,0);
						   if ((current_mode&DIPSW_SW8_MASK)!=0) {
							  vga_set_swap(VGA_QuarterScreen);
							  vga_set_pointer(image);
						   }

						   alt_u32 end_sobel_x = alt_timestamp();
						   printf("Sobel RGB : %d\n", (int)(end_sobel_x - start_sobel_x)/(CAM_SIZE_X*CAM_SIZE_Y));
					   }

		      	  	   break;
		      case 3 :
		               {
		            	   alt_timestamp_start();
						   alt_u32 start_sobel_x = alt_timestamp();

		            	   conv_grayscale((void *)image,
										  CAM_SIZE_X,
										  CAM_SIZE_Y);
						   grayscale = get_grayscale_picture();
						   sobel_x(grayscale);

						   sobel_y_with_rgb(grayscale);
						   image = GetSobel_rgb();
						   transfer_LCD_with_dma(&image[16520],
											CAM_SIZE_X,
											CAM_SIZE_Y,0);
						   if ((current_mode&DIPSW_SW8_MASK)!=0) {
							  vga_set_swap(VGA_QuarterScreen);
							  vga_set_pointer(image);
						   }


						   alt_u32 end_sobel_x = alt_timestamp();
						   printf("Sobel x : %d\n", (int)(end_sobel_x - start_sobel_x)/(CAM_SIZE_X*CAM_SIZE_Y));
		               }

		      	  	   break;
		      default:
                       {
						   alt_timestamp_start();
						   alt_u32 start_sobel_x = alt_timestamp();

						   conv_grayscale((void *)image,
							CAM_SIZE_X,
							CAM_SIZE_Y);
							grayscale = get_grayscale_picture();
						   sobel_x(grayscale);
							sobel_y(grayscale);
							sobel_threshold(128);
							grayscale=GetSobelResult();
						   transfer_LCD_with_dma(&grayscale[16520],
											CAM_SIZE_X,
											CAM_SIZE_Y,1);
						   if ((current_mode&DIPSW_SW8_MASK)!=0) {
							  vga_set_swap(VGA_QuarterScreen|VGA_Grayscale);
							  vga_set_pointer(grayscale);
						   }
						   alt_u32 end_sobel_x = alt_timestamp();
						   printf("Sobel : %d\n", (int)(end_sobel_x - start_sobel_x)/(CAM_SIZE_X*CAM_SIZE_Y));
					   }


		      	  	   break;
		      }
		  }
	  }
  } while (1);
  return 0;
}
