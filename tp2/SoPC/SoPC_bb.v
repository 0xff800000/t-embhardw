
module SoPC (
	clk_clk,
	mygpio_0_conduit_end_export,
	reset_reset_n,
	sdram_ctrl_wire_addr,
	sdram_ctrl_wire_ba,
	sdram_ctrl_wire_cas_n,
	sdram_ctrl_wire_cke,
	sdram_ctrl_wire_cs_n,
	sdram_ctrl_wire_dq,
	sdram_ctrl_wire_dqm,
	sdram_ctrl_wire_ras_n,
	sdram_ctrl_wire_we_n,
	sram_clk_clk,
	lcd2_0_conduit_end_dc,
	lcd2_0_conduit_end_rd,
	lcd2_0_conduit_end_wr,
	lcd2_0_conduit_end_data);	

	input		clk_clk;
	inout	[31:0]	mygpio_0_conduit_end_export;
	input		reset_reset_n;
	output	[11:0]	sdram_ctrl_wire_addr;
	output	[1:0]	sdram_ctrl_wire_ba;
	output		sdram_ctrl_wire_cas_n;
	output		sdram_ctrl_wire_cke;
	output		sdram_ctrl_wire_cs_n;
	inout	[15:0]	sdram_ctrl_wire_dq;
	output	[1:0]	sdram_ctrl_wire_dqm;
	output		sdram_ctrl_wire_ras_n;
	output		sdram_ctrl_wire_we_n;
	output		sram_clk_clk;
	output		lcd2_0_conduit_end_dc;
	output		lcd2_0_conduit_end_rd;
	output		lcd2_0_conduit_end_wr;
	output	[15:0]	lcd2_0_conduit_end_data;
endmodule
