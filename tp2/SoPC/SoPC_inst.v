	SoPC u0 (
		.clk_clk                     (<connected-to-clk_clk>),                     //                  clk.clk
		.mygpio_0_conduit_end_export (<connected-to-mygpio_0_conduit_end_export>), // mygpio_0_conduit_end.export
		.reset_reset_n               (<connected-to-reset_reset_n>),               //                reset.reset_n
		.sdram_ctrl_wire_addr        (<connected-to-sdram_ctrl_wire_addr>),        //      sdram_ctrl_wire.addr
		.sdram_ctrl_wire_ba          (<connected-to-sdram_ctrl_wire_ba>),          //                     .ba
		.sdram_ctrl_wire_cas_n       (<connected-to-sdram_ctrl_wire_cas_n>),       //                     .cas_n
		.sdram_ctrl_wire_cke         (<connected-to-sdram_ctrl_wire_cke>),         //                     .cke
		.sdram_ctrl_wire_cs_n        (<connected-to-sdram_ctrl_wire_cs_n>),        //                     .cs_n
		.sdram_ctrl_wire_dq          (<connected-to-sdram_ctrl_wire_dq>),          //                     .dq
		.sdram_ctrl_wire_dqm         (<connected-to-sdram_ctrl_wire_dqm>),         //                     .dqm
		.sdram_ctrl_wire_ras_n       (<connected-to-sdram_ctrl_wire_ras_n>),       //                     .ras_n
		.sdram_ctrl_wire_we_n        (<connected-to-sdram_ctrl_wire_we_n>),        //                     .we_n
		.sram_clk_clk                (<connected-to-sram_clk_clk>),                //             sram_clk.clk
		.lcd2_0_conduit_end_dc       (<connected-to-lcd2_0_conduit_end_dc>),       //   lcd2_0_conduit_end.dc
		.lcd2_0_conduit_end_rd       (<connected-to-lcd2_0_conduit_end_rd>),       //                     .rd
		.lcd2_0_conduit_end_wr       (<connected-to-lcd2_0_conduit_end_wr>),       //                     .wr
		.lcd2_0_conduit_end_data     (<connected-to-lcd2_0_conduit_end_data>)      //                     .data
	);

