-- Sandra Smialkowski
-- 23.10.2018

library IEEE;
use IEEE.STD_LOGIC_1164.all;


ENTITY LCDInterface IS
	PORT(
	-- Avalon Inerfaces signals
		Clk : IN std_logic;
		nReset : IN std_logic;
		
		Address : IN std_logic;
		ChipSelect : IN std_logic;
		
		Write : IN std_logic;
		
		Data_In : IN std_logic_vector (15 DOWNTO 0);
		WaitRequest : OUT std_logic;
		
		
	-- LCD interface signals
		--LCD_RESETn : OUT std_logic;
		--LCD_CSn : OUT std_logic;
		LCD_D_Cn : OUT std_logic;
		LCD_WRn : OUT std_logic;
		LCD_RDn : OUT std_logic;
		--LCD_IM0 : OUT std_logic;
		LCD_DATA : OUT std_logic_vector (15 DOWNTO 0)
	);
End LCDInterface;

ARCHITECTURE comp OF LCDInterface IS
	Type STATE_Type IS (Idle, Wr_Low, Wr_High);
	signal STATE, NextSTATE : STATE_Type;
BEGIN
	LCD_DATA <= Data_In;
	LCD_D_Cn <= Address;
	-- Read function is unused
	LCD_RDn <= '1';
	
Output:
	process(STATE, ChipSelect, Write)
	begin
		case STATE is
			when Idle => LCD_WRn <= '1';
				if ChipSelect = '1' and Write = '1' then
					NextSTATE <= Wr_Low;
					WaitRequest <= '1';
				else
					NextSTATE <= Idle;
					WaitRequest <= '0';
				end if;
			when Wr_Low => LCD_WRn <= '0';
				WaitRequest <= '1';
				NextSTATE <= Wr_High;
			when Wr_High => LCD_WRn <= '1';
				WaitRequest <= '0';
				NextSTATE <= Idle;
			when others => NextSTATE <= Idle;
		end case;
	end process Output;
	
StateReg :
	process(Clk, nReset)
		variable count : integer := 0;
	begin
		if nReset = '0' then
			STATE <= Idle;
			count := 0;
		elsif rising_edge(Clk) then
			if STATE = Wr_Low then
				if count < 1 then
					count:= count + 1;
				else
					STATE <= NextSTATE;
					count :=0;
				end if;
			else
				state <= NextSTATE;
			end if;
		end if;
	end process StateReg;

end comp;
		
	