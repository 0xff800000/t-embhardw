/*
 * LCDdrive.c
 *
 *  Created on: 17 oct. 2018
 *      Author: quartus
 */

#include "io.h"
#include <stdio.h>
#include "system.h"
#include "sys/alt_irq.h"

#include "d1.h"
#include "d2.h"

int counter = 0;

void timer_interrupt(void *context, alt_u32 id);
//void LCD_ctrl_interrupt(void *context, alt_u32 id);
void LCD_Write_Command(int command);
void LCD_Write_Data(int command);
void init_LCD();

int counterPeriod = 0; //100ms
int DMA_launched = 0;

// LED0 = LCD_IM0
// LED1 = LCD_nReset
// LED2 = LCD_nChipSelect


int main()
{

	//variables
	int picture[2][240*320];
	int switchPicture = 0;

	// put every GPIO in output mode
	IOWR_32DIRECT(MYGPIO_0_BASE,0,0xFFFFFFFF);

	if(IORD_32DIRECT(MYGPIO_0_BASE,0) != 0xFFFFFFFF)
	{
		printf("RegDir isn't set correctly");
		return -1;
	}

	// shut down every LED
	IOWR_32DIRECT(MYGPIO_0_BASE,2*4,0);

	printf("Lets initialize IRQ timer \n");

	alt_irq_register(TIMER_0_IRQ,(void*)2,(alt_isr_func)timer_interrupt);
	IOWR_16DIRECT(TIMER_0_BASE,4,7);  // Period defined in Qsys (100us)

	//Initialize the LCD_CTRL interrupt
	//alt_irq_register(LCD_0_BASE,(void*)2,(alt_isr_func)LCD_ctrl_interrupt);

	printf("Lets initialize our lCD screen \n");
	init_LCD();

	//convert the 2 pictures
	for(int i = 0; i < (240*320); i++)
	{
		//							B															G (right part)										G (left part)												R
		picture[0][i] = (((int)d1_image.pixel_data[2*i] & 0x1f) << 11) | (((int)d1_image.pixel_data[2*i] & 0xe0)) | (((int)d1_image.pixel_data[2*i+1] & 0x7) << 8) | (((int)d1_image.pixel_data[2*i+1] & 0xf8) >> 3);
		picture[1][i] = (((int)d2_image.pixel_data[2*i] & 0x1f) << 11) | (((int)d2_image.pixel_data[2*i] & 0xe0)) | (((int)d2_image.pixel_data[2*i+1] & 0x7) << 8) | (((int)d2_image.pixel_data[2*i+1] & 0xf8) >> 3);
	}
/*
	while(1){

		// variables for time measure
		int counter_start_LCD = 0;
		int counter_stop_LCD = 0;

		// begin of the time measure
		counter_start_LCD = counter;

		//Start the writing on the LCD
		LCD_Write_Command(0x002C);
		LCD_Write_Data(0);

		// For waiting for the end of writing
		DMA_launched = 1;

		IOWR_32DIRECT(LCD_0_BASE, 12, picture[switchPicture]); // Give source address of the picture
		IOWR_32DIRECT(LCD_0_BASE, 16, 240*320); 				// Give length of the picture and start DMA
		IOWR_32DIRECT(LCD_0_BASE, 8,0xFFFF);
		//IOWR_32DIRECT(DMA_LCD_CTRL_0_BASE, 8,0);
		while (DMA_launched == 1);


		// end of the time measure
		counter_stop_LCD = counter;

		//Wait for 1 second
		while (counter < (counter_start_LCD + 10000));
		switchPicture++;

		// Change the picture for the next displaying
		if (switchPicture >= 2)
		{
			switchPicture -= 2;
		}

		//Print the picture displayed
		printf("Picture = nyan%d\n", switchPicture + 1);
	}
*/
	for(int n=0;;n++){
		int t1 = counter,t2;
		LCD_Write_Command(0x002C);
		for(int i=0; i < 320; i++){
			for(int j=0; j < 240; j++){
				//BGR (5,6,5)
				//LCD_Write_Data(picture[i%2]);
				LCD_Write_Data(picture[n%2][i*240+j]);
				//LCD_Write_Data((n%2)? 0xffffffff : 0);
				//LCD_Write_Data(i);
			}
		}
		t2 = counter;
		int time_elapsed = (t2-t1)*10;
		printf("Image drawn in %i ms\n", time_elapsed);
	}


	while(1){}
}

void timer_interrupt(void *context, alt_u32 id){
	counter ++; // increase the counter;
	// write counter value on the parallel port;
		//printf("counter = %d\n",counter);
		//IOWR_32DIRECT(GPIO_AVALON_HW_0_BASE,2*4,counter);
	// acknowledge IRQ on the timer;
	IOWR_16DIRECT(TIMER_0_BASE,0,0);
}

/*
void LCD_ctrl_interrupt(void *context, alt_u32 id)
{
	DMA_launched = 0; // End of writing -> stop waiting
	IOWR_16DIRECT(LCD2_0_BASE, 8,2); // Acknowledge IRQ
	IOWR_16DIRECT(LCD2_0_BASE, 8,0);
}
*/
//LCD functions
// Please READ THE COMMENTS!!!

void init_LCD() {

	// LED0 = LCD_IM0			= 1
	// LED1 = LCD_nReset		= 2
	// LED2 = LCD_nChipSelect	= 4

     IOWR_32DIRECT(MYGPIO_0_BASE,2*4,0x00000004); // set reset on and 16 bits mode
     while (counter<150){}   // include delay of at least 120 ms use your timer or a loop
     IOWR_32DIRECT(MYGPIO_0_BASE,2*4,0x00000002); // set reset off and 16 bits mode and enable LED_CS
     while (counter<250){}   // include delay of at least 120 ms use your timer or a loop

      LCD_Write_Command(0x0028);     //display OFF
      LCD_Write_Command(0x0011);     //exit SLEEP mode
      LCD_Write_Data(0x0000);

      LCD_Write_Command(0x00CB);     //Power Control A
      LCD_Write_Data(0x0039);     //always 0x39
      LCD_Write_Data(0x002C);     //always 0x2C
      LCD_Write_Data(0x0000);     //always 0x00
      LCD_Write_Data(0x0034);     //Vcore = 1.6V
      LCD_Write_Data(0x0002);     //DDVDH = 5.6V

      LCD_Write_Command(0x00CF);     //Power Control B
      LCD_Write_Data(0x0000);     //always 0x00
      LCD_Write_Data(0x0081);     //PCEQ off
      LCD_Write_Data(0x0030);     //ESD protection

      LCD_Write_Command(0x00E8);     //Driver timing control A
      LCD_Write_Data(0x0085);     //non - overlap
      LCD_Write_Data(0x0001);     //EQ timing
      LCD_Write_Data(0x0079);     //Pre-charge timing


      LCD_Write_Command(0x00EA);     //Driver timing control B
      LCD_Write_Data(0x0000);        //Gate driver timing
      LCD_Write_Data(0x0000);        //always 0x00

      LCD_Write_Command(0x00ED); //Power‐On sequence control
      LCD_Write_Data(0x0064);        //soft start
      LCD_Write_Data(0x0003);        //power on sequence
      LCD_Write_Data(0x0012);        //power on sequence
      LCD_Write_Data(0x0081);        //DDVDH enhance on

      LCD_Write_Command(0x00F7);     //Pump ratio control
      LCD_Write_Data(0x0020);     //DDVDH=2xVCI

      LCD_Write_Command(0x00C0);    //power control 1
      LCD_Write_Data(0x0026);
      LCD_Write_Data(0x0004);     //second parameter for ILI9340 (ignored by ILI9341)

      LCD_Write_Command(0x00C1);     //power control 2
      LCD_Write_Data(0x0011);

      LCD_Write_Command(0x00C5);     //VCOM control 1
      LCD_Write_Data(0x0035);
      LCD_Write_Data(0x003E);

      LCD_Write_Command(0x00C7);     //VCOM control 2
      LCD_Write_Data(0x00BE);

      LCD_Write_Command(0x00B1);     //frame rate control
      LCD_Write_Data(0x0000);
      LCD_Write_Data(0x0010);

      LCD_Write_Command(0x003A);    //pixel format = 16 bit per pixel
      LCD_Write_Data(0x0055);

      LCD_Write_Command(0x00B6);     //display function control
      LCD_Write_Data(0x000A);
      LCD_Write_Data(0x00A2);

      LCD_Write_Command(0x00F2);     //3G Gamma control
      LCD_Write_Data(0x0002);         //off

      LCD_Write_Command(0x0026);     //Gamma curve 3
      LCD_Write_Data(0x0001);

      LCD_Write_Command(0x0036);     //memory access control = BGR
      LCD_Write_Data(0x0000);

      LCD_Write_Command(0x002A);     //column address set
      LCD_Write_Data(0x0000);
      LCD_Write_Data(0x0000);        //start 0x0000
      LCD_Write_Data(0x0000);
      LCD_Write_Data(0x00EF);        //end 0x00EF

      LCD_Write_Command(0x002B);    //page address set
      LCD_Write_Data(0x0000);
      LCD_Write_Data(0x0000);        //start 0x0000
      LCD_Write_Data(0x0001);
      LCD_Write_Data(0x003F);        //end 0x013F

      LCD_Write_Command(0x0029);

  }

  void LCD_Write_Command(int command) {
      IOWR_16DIRECT(LCD2_0_BASE,0*2,command);
      //0 = offset for command
  }

  void LCD_Write_Data(int data) {
      IOWR_16DIRECT(LCD2_0_BASE,1*2,data);
      //1 = offset for data
  }

